import requests
import pandas as pd
import time

token = ''  # Gitee Token Here

limit = 100
prev_id = 185919465
actor_dict = {}
events_list = []
i = 0

while prev_id > 1000000:
    # 10000条存一次
    print(f"Getting events from {prev_id}...")
    event_list_payload = {'access_token': token,
                          'prev_id': prev_id,
                          'limit': limit
                          }
    r = requests.get('https://gitee.com/api/v5/orgs/openeuler/events', params=event_list_payload)
    events = r.json()

    if not events:
        print('——————————————————Saving event list——————————————————')
        events_df = pd.DataFrame(events_list)
        events_df.to_csv(f'data/events_data/events({prev_id}).csv', index=False)
        events_list = []
        i = 0
        print(actor_dict)
        actor_series = pd.Series(actor_dict)
        actor_series.index.name = 'user_id'
        actor_series.to_csv(f'data/actors_data/actor_list({prev_id}).csv', header=['user_login'])

    prev_id = events[-1]['id']
    print(f"Now prev_id: {prev_id}")

    for event in events:
        actor_dict.update({event['actor']['id']: event['actor']['login']})

    events_list = events_list + events

    i += 1

    if i == 100:
        print('——————————————————Saving event list——————————————————')
        events_df = pd.DataFrame(events_list)
        events_df.to_csv(f'data/events_data/events({prev_id}).csv', index=False)
        events_list = []
        i = 0
        print(actor_dict)
        actor_series = pd.Series(actor_dict)
        actor_series.index.name = 'user_id'
        actor_series.to_csv(f'data/actors_data/actor_list({prev_id}).csv', header=['user_login'])

    print('Sleeping for 30s!!!')
    time.sleep(30)

# User list save
actor_series = pd.Series(actor_dict)
actor_series.index.name = 'user_id'
actor_series.to_csv('data/actor_list.csv', header=['user_login'])
