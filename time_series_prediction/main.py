from prediction import time_series_prediction
from elasticsearch import Elasticsearch
from datetime import datetime
from sklearn import preprocessing
from prophet import Prophet
import numpy as np
import pandas as pd
import os
import json
import yaml

# event数据输出入口，需要将待分析时间段的event数据列表存入openeuler_events DataFrame中
openeuler_events = pd.read_csv('data/events_data/openeuler_events.csv')


# SIG yaml 解析
def get_yaml_data(yaml_file):
    file = open(yaml_file, 'r', encoding="utf-8")
    file_data = file.read()
    file.close()
    data = yaml.load(file_data, Loader=yaml.FullLoader)
    return data


current_path = os.path.abspath(".")
yaml_path = os.path.join(current_path, "sigs.yaml")
sig_data = get_yaml_data(yaml_path)

sig_list = {}
for sig in sig_data['sigs']:
    for repo in sig['repositories']:
        sig_list[repo] = sig['name']
# print(sig_list)

# 各事件权值定义 目前根据经验确定 在线部署后可根据社区变化动态调整
user_popularity_weight = {'PullRequestCommentEvent':1,
                          'PushEvent':2,
                          'PullRequestEvent':2,
                          'DeleteEvent':0.5,
                          'CreateEvent':0.5,
                          'IssueCommentEvent':1,
                          'MemberEvent':1,
                          'IssueEvent':2,
                          'ProjectCommentEvent':1,
                          'CommitCommentEvent':2,
                          'MilestoneEvent':1}
sig_popularity_weight =  {'PullRequestCommentEvent':1,
                          'PushEvent':2,
                          'PullRequestEvent':2,
                          'DeleteEvent':0.5,
                          'CreateEvent':0.5,
                          'IssueCommentEvent':1,
                          'MemberEvent':1,
                          'IssueEvent':2,
                          'ProjectCommentEvent':1,
                          'CommitCommentEvent':2,
                          'MilestoneEvent':1}

# 对原始数据进行处理，计算开发者和SIG组的历史活跃度
user_time_series = []
sig_time_series = []

for index, event in openeuler_events.iterrows():
    actor = eval(event['actor'])
    repo = eval(event['repo'])

    # 活跃度计算
    if isinstance(event['type'], 'str'):
        user_activity = 0
        sig_activity = 0
    else:
        user_activity = user_popularity_weight[event['type']]
        sig_activity = sig_popularity_weight[event['type']]

    # 时间序列数据生成模块 根据log列表生成活跃度时间序列
    date_month = event['created_at'][0:7]

    # 生成用户时间序列活跃度数据
    user_action = {'actor_id': actor['id'],
                   'actor_login': actor['login'],
                   'actor_name': actor['name'],
                   'date': date_month,
                   'activity_metric': user_activity,
                   'repo_comp': '',
                   'action_comp': '',
                   'label': 0,  # 原始数据非预测值
                   }
    # print(user_action)
    user_time_series.append(user_action)

    # 生成SIG组时间序列活跃度数据
    if repo['full_name'] not in sig_list:
        continue
    else:
        sig_name = sig_list[repo['full_name']]

    sig_action = {'sig_name': sig_name,
                  'date': date_month,
                  'activity_metric': sig_activity,
                  'repo_comp': '',
                  'action_comp': '',
                  'label': 0,  # 原始数据非预测值
                  }
    # print(sig_action)
    sig_time_series.append(sig_action)

user_time_series_pd = pd.DataFrame(user_time_series)
sig_time_series_pd = pd.DataFrame(sig_time_series)
users = user_time_series_pd.groupby(['actor_login', 'date']).sum('popularity').sort_values('date')
sigs = sig_time_series_pd.groupby(['sig_name', 'date']).sum('popularity').sort_values('date')
users = users.reset_index()
sigs = sigs.reset_index()

print(users)
print(sigs)

# 活跃度时间序列预测，调用prediction.py里的预测函数
sigs = time_series_prediction(sigs)
users = time_series_prediction(users)

# Elastic Search 数据输出模块
# 数据输出模块 pandas_dataframe 可以存入各类数据源


def connect_es(frame, index_, type_):
    try:
        es = Elasticsearch()
        df_as_json = frame.to_json(orient='records', lines=True)
        bulk_data = []

        for json_document in df_as_json.split('\n'):
            bulk_data.append({"index": {
                '_index': index_,
                '_type': type_,
            }})
            bulk_data.append(json.loads(json_document))

            # 一次bulk request包含1000条数据
            if len(bulk_data) > 1000:
                es.bulk(bulk_data)
                bulk_data = []

        es.bulk(bulk_data)
        print('database connect successfully')

    except Exception as e:

        print(e)


# es_index = 'openeuler-user' # "openeuler-user" or "openeuler-sig"
# es_type = '_doc'
connect_es(sigs, 'openeuler-sig', '_doc')
connect_es(users, 'openeuler-user', '_doc')
