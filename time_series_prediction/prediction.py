from prophet import Prophet
from datetime import datetime


def time_series_predict(time_series_data):
    time_series = Prophet(seasonality_mode='multiplicative').fit(time_series_data)
    future = time_series.make_future_dataframe()
    future_time_series_data = time_series.predict(future)
    return future_time_series_data


def time_series_prediction(time_series_data):
    return time_series_data
